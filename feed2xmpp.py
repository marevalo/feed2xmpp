#!/usr/bin/env python3

import time

import asyncio
import feedparser
import pickle

from publishx import Publishx
import config

import logging
import importlib as imp

from termcolor import colored
from bs4 import BeautifulSoup, Comment
from xml.etree import ElementTree

def setup_logging(level):
    log = logging.getLogger('atomtopubsub')
    log.setLevel(level)
    ch = logging.StreamHandler()
    ch.setLevel(level)
    formatter = logging.Formatter('%(message)s')
    ch.setFormatter(formatter)
    log.addHandler(ch)


# We feed the pubsub nodes
async def parse(parsed, xmpp):
    imp.reload(config)

    # We parse all the feeds
    for key, feed in config.feeds.items():
        print(colored('>> parsing %s' % key, 'magenta'))
        f = feedparser.parse(feed['url'])

        if f.bozo == 1:
            print('XML Error')
            if hasattr(f.bozo_exception, 'getMessage'):
                print(f.bozo_exception.getMessage())
            if hasattr(f.bozo_exception, 'getLineNumber'):
                print('at line %s' % f.bozo_exception.getLineNumber())

        if feed['type'] == 'pubsub' and key not in parsed:
            title = description = ''
            if hasattr(f, 'title'):
                title = f.title
                if hasattr( f , 'subtitle'):
                    description = f.subtitle
            await xmpp.create( feed['target'], key, title, description)

        # create incremental record date for new entries
        # ok, I will asume no feed before 1970
        last_updated_entry = time.gmtime(0)
        if key in parsed:
            last_updated_entry = parsed[key]

        # We check if we have some new entries
        for entry in reversed(f.entries):
            atom_entry = {}
            if 'flags' in feed.keys() and 'link_is_id' in feed['flags']:
                atom_entry['id'] = entry.links[0].href
            else:
                atom_entry['id'] = entry.id
            atom_entry['title'] = entry.title
            atom_entry['updated'] = entry.updated
            if key not in parsed or parsed[key] < entry.updated_parsed:
                print(colored('++ new entry %s' % entry.title, 'green'))
                # process the content section for RSS
                if not hasattr(entry , 'content' ) and hasattr( entry , 'description' ):
                    atom_entry['content'] = {}
                    atom_entry['content']['type'] = 'xhtml'
                    soup = BeautifulSoup(entry.description, 'html.parser')

                    atom_entry['content']['value'] = (
                            '<div xmlns="http://www.w3.org/1999/xhtml">\n%s\n</div>' % soup.prettify()
                    ).splitlines()

                # process the content section for Atom
                if hasattr( entry , 'content' ) and hasattr(entry.content[0], 'type'):
                    atom_entry['content'] = {}
                    # soup = BeautifulSoup(entry.content[0].value, 'lxml')
                    soup = BeautifulSoup(entry.content[0].value, 'html.parser')
                    atom_entry['content']['type'] = 'xhtml'

                    comments = soup.findAll(text=lambda text:isinstance(text, Comment))

                    for comment in comments:
                        comment.extract()

                    atom_entry['content']['value'] = (
                            '<div xmlns="http://www.w3.org/1999/xhtml">\n%s\n</div>' % soup.prettify()
                    ).splitlines()

                # Validate the content value as valid XML
                try:
                    ElementTree.fromstring(''.join(atom_entry['content']['value']))
                except Exception as e:
                    print(e)
                    print(colored('-- XML parsing failed for %s' %  entry.title, 'red'))
                    continue

                # process the links section
                if hasattr(entry, 'links'):
                    atom_entry['links'] = []
                    for l in entry.links:
                        atom_entry['links'].append( l )

                # process the tags section
                if hasattr(entry, 'tags'):
                    atom_entry['tags'] = []
                    for t in entry.tags:
                        atom_entry['tags'].append( t.term )

                # process the authors section
                if hasattr(entry, 'authors'):
                    atom_entry['author'] = {}
                    atom_entry['author']['name'] = entry.authors[0].name
                    if hasattr(entry.authors[0], 'href'):
                        atom_entry['author']['href'] = entry.authors[0].href

                if feed['type'] == 'pubsub':
                    await xmpp.publish(feed['target'], key, atom_entry)
                elif feed['type'] == 'chat' or feed['type'] == 'muc':
                    message = ''
                    message += atom_entry['title']
                    if 'flags' in feed.keys() and 'chat_include_key' in feed['flags']:
                        message = key + " : " + message
                    if 'links' in atom_entry.keys() and (
                            'flags' not in feed.keys() or
                            'chat_exclude_link' not in feed['flags']):
                        message += " - " + atom_entry['links'][0]['href']
                    if 'flags' in feed.keys() and 'chat_include_body' in feed['flags']:
                        message += "\n" + soup.get_text()
                    if feed['type'] == 'chat':
                        await xmpp.send_chat( feed['target'] , message )
                    elif feed['type'] == 'muc':
                        await xmpp.send_groupchat(feed['target'], config.resource , message)
            else:
                print(colored('++ Not updated entry %s' % entry.title, 'yellow'))
            if hasattr( entry , "updated_parsed") and entry.updated_parsed > last_updated_entry:
                last_updated_entry = entry.updated_parsed

        # And we update the last updated date for the feed
        if 'flags' in feed.keys() and 'ignore_feed_date' in feed['flags']:
            parsed[ key ] = last_updated_entry
        elif hasattr( f.feed , 'updated_parsed'):
            parsed[key] = f.feed.updated_parsed
        elif last_updated_entry != time.gmtime(0):
            parsed[key] = last_updated_entry
        else:
            # one last try, probably won't be good for anything as we cannot compare with entry's update time
            parsed[key] = time.gmtime()

        save(parsed)

        # We distribute the parsing
        minutes = float(config.refresh_time) / len(config.feeds)
        print(colored('Parsing next feed in %.2f minutes' % minutes, 'cyan'))
        await asyncio.sleep(minutes * 60)
    asyncio.ensure_future(parse(parsed, xmpp))


def load():
    try:
        pkl_file = open('cache.pkl', 'rb')
        parsed = pickle.load(pkl_file)
        pkl_file.close()
        return parsed
    except IOError:
        print('Creating the cache')
        return save({})


def save(parsed):
    output = open('cache.pkl', 'wb')
    pickle.dump(parsed, output)
    output.close()
    return {}


def main():
    setup_logging(logging.INFO)

    xmpp = Publishx(config)
    xmpp.connect()
    xmpp.add_event_handler('session_start', lambda _: asyncio.ensure_future(parse(load(), xmpp)))
    xmpp.process()


if __name__ == '__main__':
    main()
