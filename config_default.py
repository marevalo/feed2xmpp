# Feed Settings

feeds = {
        'FeedName1' : {                                                 # Change this to the name of the group you want to add this feed to
            'url' : 'http://localhost/feedcleaner/?url=FEEDGOESHERE',   # Replace "FEEDGOESHERE" with the URL to the Atom Feed, leave the rest of of the line intact, see the example below
            'target' : 'pubsubserver',                                   # Change this to the target JID (can be a pubsub node or XMPP address
            'type'  : 'pubsub' ,
            'flags'  : [ ]
        },
        # Add more feeds as you need them using this format
        #'FeedName2' : {
        #    'url' : 'http://localhost/feedcleaner/?url=FEEDGOESHERE',
        #    'target' : 'jid',
        #    'type': 'pubsub' | 'chat' | "muc",
        #    'flags' : [ 'link_is_id' , ... ]
        #    },
        # Remember to add a comma ',' after every close bracket except for the last one
        # You can add these flags to fix some feeds:
        #     link_is_id : some feeds don't include a valid id for entries, use link instead
        #     ignore_feed_date : some feeds publish elements in dates older than last feed date,
        #                        with this option it will only use the last entry date for checking new entries
        #     chat_include_body : on chat type targets send also the full content of the entry in plain text
        #     chat_include_key : on chat type targets send the key before the title
        #     chat_exclude_link : on chat type targets do not send the link after the title (for twitter-like feeds)
        'Feedname3' : {
            'url' : 'http://localhost/feedcleaner/?url=http://feedurlhere/rss.xml',
            'target' : 'romeo@capulet.lit',
            'type'  : 'chat' ,
            'flags'  : [ ]
            }
    }

# XMPP Authentication Settings

jid         = 'user@server.tld' # XMPP UserID and domain to post the feeds as
resource    = 'atomtopubsub'    # You can change this if you want, it only tells XMPP what application posted the feeds
secret      = 'password'        # The password for the above UserID

# Refresh intervals in minutes
refresh_time = 60
# The refresh time will be split evenly over all of your feeds.
# Eg. 60 minutes across 3x feeds, will equal 1x feed will be refreshed every 20 minutes.
# Or 60 minutes across 6x feeds would equal 1x feed will be refreshed every 10 minutes.
