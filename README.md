# feed2xmpp

feed2xmpp is a python script that will retrieve feeds (in almost
any format available) and publish the posts on the XMPP network
be it by PubSub or sending them to a user or MUC chatroom.

feed2xmpp is a for of edhelas' already great 
[atomtopubsub](https://github.com/edhelas/atomtopubsub) that tries to
increase the formats supported and publication target.

## Requirements

feed2xmpp needs all these dependencies:

* Python >= 3.5 ( I suppose... no idea about real version needs)
* feedparser
* slixmpp
* jsonpickle
* bs4
* termcolor (by the moment, probably will drop it in the future)


## Installation

Install python and all dependencies and create a config.py file on
the current directory.

## Configuration

You should create a config.py (you can base it on the config_defaul.py one
and modify it to suit your needs:

### General settings

This part will modify how the script will connect to the XMPP network
and some general configurations:

```
# XMPP Authentication Settings

jid         = 'user@server.tld' # XMPP UserID and domain to post the feeds as
resource    = 'atomtopubsub'    # You can change this if you want, it only tells XMPP what application posted the feeds
secret      = 'password'        # The password for the above UserID

# Refresh intervals in minutes
refresh_time = 60   # The refresh time will be split evenly over all of your feeds
```

### Feeds configuration

You can add as many feeds as you want on the configuration like this:

```
feeds = {
        'FeedName1' : {
            'url' : 'http://...',
            'target' : 'pubsub.capulet.lit',
            'type'  : 'pubsub' ,
            'flags'  : [ ]
            } ,
        'Feedname2' : {
            'url' : 'http://...',
            'target' : 'romeo@capulet.lit',
            'type'  : 'chat' ,
            'flags'  : [ 'chat_include_body' ]
            } ,
        'Feedname3' : {
            'url' : 'http://...',
            'target' : 'balcony@chat.shakespeare.lit',
            'type'  : 'muc' ,
            'flags'  : [ 'chat_include_key' , 'chat_exclude_link' ]
            }
    }
```

Each feed has this variables:

* **url** : The URL of the feed
* **target** : Service for pubsub / jid for user / mucroom for muc
* **type** : 'pubsub' | 'chat' | "muc"
* **flags** : any of this posible flags to "fix" or modify the feed:
  * **link_is_id** : some feeds don't include a valid id for entries, use
  link instead
  * **ignore_feed_date** : some feeds publish elements in dates older than
  last feed date, with this option it will only use the last entry date
  for checking new entries
  * **chat_include_body** : on chat type targets send also the full content
  of the entry in plain text
  * **chat_include_key** : on chat type targets send the key before the
  title
  * **chat_exclude_link** : on chat type targets do not send the link after
  the title (for twitter-like feeds)

## Running

Well, just run it.

## Development, new features and fixes

Development is done in the public on gitlab, where you can download
new versions and request new features. If you find feeds that it cannot
process (be it an bug on feed2xmpp or on the feed) please add the
URL of the feed on the request.

[feed2xmpp Gitlab page](https://gitlab.com/marevalo/feed2xmpp)
