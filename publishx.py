from termcolor import colored

import slixmpp
from slixmpp.xmlstream import ET
import slixmpp.plugins.xep_0060.stanza.pubsub as pubsub
from slixmpp.exceptions import IqError, IqTimeout
import re

NS_ATOM = 'http://www.w3.org/2005/Atom'
NS_JABBER_DATA = 'jabber:x:data'

class Publishx(slixmpp.ClientXMPP):
    def __init__(self, config):
        jid = config.jid
        fulljid = config.jid + "/" + config.resource
        secret = config.secret
        resource = config.resource

        slixmpp.ClientXMPP.__init__(self, fulljid, secret)
        # On some servers in order to send messages we need to send presence and get the roster
        #self.send_presence()
        #self.get_roster()
        # Register que PubSub plugin
        self.register_plugin('xep_0060')
        # Register que MUC plugin
        self.register_plugin('xep_0045')

    async def create(self, server, node, title , description ):
        print(colored('>> create %s' % title, 'blue'))

        iq = self.Iq(stype="set", sto=server)
        iq['pubsub']['create']['node'] = node
        form = iq['pubsub']['configure']['form']
        form['type'] = 'submit'
        form.addField('pubsub#persist_items',
                      ftype='boolean',
                      value=1)
        form.addField('pubsub#title',
                      ftype='text-single',
                      value=title)
        form.addField('pubsub#max_items',
                      ftype='text-single',
                      value='20')
        form.addField('pubsub#type',
                      ftype='text-single',
                      value=NS_ATOM)
        form.addField('pubsub#deliver_payloads',
                      ftype='boolean',
                      value=0)
        form.addField('pubsub#description',
                      ftype='text-single',
                      value=description)

        task = iq.send(timeout=5)
        try:
            await task
        except IqError as e:
            if e.etype == 'cancel' and e.condition == 'conflict':
                print(colored('!! node %s is already created, assuming its configuration is correct' % node, 'yellow'))
                return
            raise

    # entry needs not be a feedparser entry anymore, as we publish atom entries it is an atom dictionary with:
    # - entry[id]
    # - entry[title]
    # - entry[updated]
    # - entry[content][type]
    # - entry[content][value]
    # - entry[links][n][href]
    # - entry[links][n][type]
    # - entry[links][n][rel]
    # - entry[tags][n]
    # - entry[author][name]
    # - entry[author][href]
    # why a dictionary ? I want to make it separate from feedparser and don't want to create a new class
    async def publish(self, server, node, entry):
        iq = self.Iq(stype="set", sto=server)
        iq['pubsub']['publish']['node'] = node

        item = pubsub.Item()
        # character / is causing a bug in movim. replacing : and , with - in id. It provides nicer urls.
        rex = re.compile(r'[:,\/]')
        item['id'] = rex.sub('-', str(entry['id']))

        ent = ET.Element("entry")
        ent.set('xmlns', NS_ATOM)

        title = ET.SubElement(ent, "title")
        title.text = entry['title']

        updated = ET.SubElement(ent, "updated")
        updated.text = entry['updated']

        if 'content' in entry.keys() :
            content = ET.SubElement(ent, "content")
            content.set('type', entry['content']['type'])
            content.text = entry['content']['value']

        if 'links' in entry.keys():
            for l in entry['links']:
                link = ET.SubElement(ent, "link")
                link.set('href', l['href'])
                link.set('type', l['type'])
                link.set('rel', l['rel'])

        if 'tags' in entry.keys():
            for t in entry['tags']:
                tag = ET.SubElement(ent, "category")
                tag.set('term', t )

        if 'author' in entry.keys():
            author = ET.SubElement(ent, "author")
            name = ET.SubElement(author, "name")
            name.text = entry['author']['name']
            if 'href' in entry['author'].keys():
                uri = ET.SubElement(author, "uri")
                uri.text = entry['author']['href']

        item['payload'] = ent

        iq['pubsub']['publish'].append(item)

        task = iq.send(timeout=30)
        try:
            await task
        except (IqError, IqTimeout) as e:
            print(e)
            pass


    async def send_chat(self, recipient , message):
        self.send_message(
                mto = recipient ,
                mbody = message ,
                mtype = 'chat'
        )


    async def send_groupchat(self, muc, nick , message):
        # we're gonna connect and disconnect to the MUC with every message: it's less efficient on the first
        # rush of elements but in the long run is better and more error-proof
        self.plugin['xep_0045'].join_muc(muc,
                                         nick,
                                         wait=True)
        self.send_message(mto=muc,
                          mbody= message,
                          mtype='groupchat')
        self.plugin['xep_0045'].leave_muc(muc,
                                         nick)

    def published(self):
        print('published')
